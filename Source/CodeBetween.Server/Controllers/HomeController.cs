﻿using System.Web.Mvc;

namespace CodeBetween.Server.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "CodeBetween Home";
            return View();
        }

        [AllowAnonymous]
        public ActionResult SignUp()
        {
            ViewBag.Title = "CodeBetween Sign Up";
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            ViewBag.Title = "CodeBetween Login";
            return View();
        }
    }
}
