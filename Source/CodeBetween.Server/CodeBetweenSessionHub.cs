﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.AspNet.SignalR;

using CodeBetween.Lib;

namespace CodeBetween.Server
{
    [Authorize]
    public class CodeBetweenSessionHub : Hub
    {
        public void SessionUpdate(string sessionId, TextUpdate update)
        {
            Clients.OthersInGroup(sessionId).sessionUpdate(update);
        }

        public void SubscribeToSession(string sessionId)
        {
            Groups.Add(Context.ConnectionId, sessionId);
        }
    }
}