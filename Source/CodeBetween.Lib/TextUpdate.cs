﻿namespace CodeBetween.Lib
{
    public class TextUpdate
    {
        /// <summary>
        /// Text that was added.
        /// </summary>
        public string Added { get; set; }

        /// <summary>
        /// The index in buffer where the new text was added, after any remove took place.
        /// </summary>
        public int AddedAt { get; set; }

        /// <summary>
        /// Text that was removed.
        /// </summary>
        public string Removed { get; set; }

        /// <summary>
        /// The index in buffer where old text was removed, before the remove took place.
        /// When applying an update it's important Remove comes before Insert.
        /// </summary>
        public int RemovedAt { get; set; }

    }
}
