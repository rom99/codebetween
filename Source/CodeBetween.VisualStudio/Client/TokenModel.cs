﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeBetween.VisualStudio.Client
{
    internal class TokenModel
    {
        /// <summary>
        /// The access token to use.
        /// </summary>
        [JsonProperty(PropertyName="access_token")]
        internal string AccessToken { get; set; }

        /// <summary>
        /// When the access token is due to expire.
        /// </summary>
        [JsonProperty(PropertyName=".expires")]
        internal DateTime Expires { get; set; }

        /// <summary>
        /// Whether the access token has already expired.
        /// </summary>
        [JsonIgnore]
        internal bool Expired
        {
            get
            {
                return DateTime.UtcNow > Expires;
            }
        }
    }
}
