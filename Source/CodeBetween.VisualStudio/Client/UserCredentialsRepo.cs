﻿using System.IO;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using CodeBetween.Lib;

namespace CodeBetween.VisualStudio.Client
{
    internal class UserCredentialsRepo
    {
        private IConfiguration _config;

        internal UserCredentialsRepo(IConfiguration config)
        {
            _config = config;
        }

        internal async Task<UserCredentials> GetAsync()
        {
            using (var file = File.Open(_config.CredentialsPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var textReader = new StreamReader(file, Encoding.UTF8))
            {
                string encryptedValue = await textReader.ReadToEndAsync();
                string decryptedValue = EncryptionTool.Decrypt(encryptedValue, _config.EncryptionPassPhrase);
                return JsonConvert.DeserializeObject<UserCredentials>(decryptedValue);
            }
        }

        internal async Task SaveAsync(UserCredentials credentials)
        {
            string serialisedCredentials = JsonConvert.SerializeObject(credentials);
            string encryptedValue = EncryptionTool.Encrypt(serialisedCredentials, _config.EncryptionPassPhrase);
            using (var file = File.Open(_config.CredentialsPath, FileMode.Create, FileAccess.Write, FileShare.None))
            using (var textWriter = new StreamWriter(file, Encoding.UTF8))
            {
                await textWriter.WriteAsync(encryptedValue);
            }
        }
    }
}
