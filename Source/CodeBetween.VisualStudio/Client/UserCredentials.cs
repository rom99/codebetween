﻿using CodeBetween.Lib;
namespace CodeBetween.VisualStudio.Client
{
    internal class UserCredentials
    {
        internal string Username { get; private set; }

        internal string Password { get; private set; }

        internal UserCredentials(string username, string password)
        {
            Username = username;
            Password = password;
        }
    }
}
