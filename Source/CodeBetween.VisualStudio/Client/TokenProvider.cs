﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CodeBetween.VisualStudio.Client
{
    /// <summary>
    /// Holds result of token refresh action, either containing the newly minted Token
    /// or some error message if unsuccessful.
    /// </summary>
    internal class TokenRefreshResult
    {
        /// <summary>
        /// The new Token, null if we failed to obtain one.
        /// </summary>
        internal TokenModel Token { get; private set; }

        /// <summary>
        /// True if a new Token was obtained.
        /// </summary>
        internal bool Success
        {
            get
            {
                return Token != null;
            }
        }

        /// <summary>
        /// Whether a ResponseError exists.
        /// </summary>
        internal bool HasResponseError
        {
            get
            {
                return ResponseError != null;
            }
        }

        /// <summary>
        /// Whether a RequestError exists.
        /// </summary>
        internal bool HasRequestError
        {
            get
            {
                return RequestError != null;
            }
        }

        /// <summary>
        /// The response error, null if no response error occurred.
        /// </summary>
        internal string ResponseError
        {
            get; private set;
        }

        /// <summary>
        /// The request error, null if no request error occurred.
        /// </summary>
        internal string RequestError
        {
            get; private set;
        }

        /// <summary>
        /// Creates a new TokenRefreshResult.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="responseError"></param>
        /// <param name="requestError"></param>
        internal TokenRefreshResult(
            TokenModel token = null, 
            string responseError = null, 
            string requestError = null)
        {
            Token = token;
            ResponseError = responseError;
            RequestError = requestError;
        }
    }

    internal class TokenRefreshErrorModel
    {
        [JsonProperty(PropertyName = "error")]
        internal string Error { get; set; }

        [JsonProperty(PropertyName = "error_description")]
        internal string ErrorDescription { get; set; }
    } 

    /// <summary>
    /// Service for obtaining CodeBetween access tokens.
    /// </summary>
    internal class TokenProvider
    {
        private IConfiguration _configuration;

        /// <summary>
        /// The Token.
        /// </summary>
        internal TokenModel Token { get; private set; }

        /// <summary>
        /// Create a TokenProvider with the provided configuration.
        /// </summary>
        /// <param name="config"></param>
        internal TokenProvider(IConfiguration config)
        {
            _configuration = config;
        }

        /// <summary>
        /// Refresh the access token.
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        internal async Task<TokenRefreshResult> RefreshAsync(UserCredentials credentials)
        {
            var client = new HttpClient();
            HttpResponseMessage response; 
            try
            {
                response = await client.PostAsync(
                    _configuration.CodeBetweenTokenEndpoint,
                    new FormUrlEncodedContent(new Dictionary<string, string>
                    {
                        ["username"] = credentials.Username,
                        ["password"] = credentials.Password,
                        ["grant_type"] = "password"
                    }));
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return new TokenRefreshResult(requestError: ex.Message);
            }
            if (response.IsSuccessStatusCode)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                Token = JsonConvert.DeserializeObject<TokenModel>(responseContent);
                return new TokenRefreshResult(token: Token);
            }
            else
            {
                try
                {
                    string responseContent = await response.Content.ReadAsStringAsync();
                    TokenRefreshErrorModel error = JsonConvert.DeserializeObject<TokenRefreshErrorModel>(responseContent);
                    return new TokenRefreshResult(responseError: error.ErrorDescription);
                }
                catch(Exception ex)
                {
                    return new TokenRefreshResult(responseError: ex.Message);
                }
            }
        }
    }
}
