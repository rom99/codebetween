﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeBetween.VisualStudio.Client
{
    internal interface IConfiguration
    {
        string CredentialsPath { get; set; }

        string CodeBetweenTokenEndpoint { get; set; }

        string EncryptionPassPhrase { get; set; }
    }
}
