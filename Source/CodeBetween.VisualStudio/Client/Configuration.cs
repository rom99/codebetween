﻿namespace CodeBetween.VisualStudio.Client
{
    /// <summary>
    /// Application configuration and settings
    /// </summary>
    internal class Configuration: IConfiguration
    {
        public string CredentialsPath { get; set; }

        public string CodeBetweenTokenEndpoint { get; set; }

        public string EncryptionPassPhrase { get; set; }
    }
}
