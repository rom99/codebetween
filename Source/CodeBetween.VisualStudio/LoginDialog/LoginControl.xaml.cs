﻿using CodeBetween.VisualStudio.Client;
using System;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace CodeBetween.VisualStudio.LoginDialog
{
    /// <summary>
    /// Interaction logic for LoginControl.xaml
    /// </summary>
    public partial class LoginControl : UserControl
    {
        IConfiguration _config;
        TokenProvider _tokenProvider;

        internal event Action OnLoggedIn;

        internal LoginControl(IConfiguration config, TokenProvider tokenProvider)
        {
            _config = config;
            _tokenProvider = tokenProvider;
            InitializeComponent();
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        private async void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            string username = UsernameField.Text;
            string password = passwordField.Password;
            var credentials = new UserCredentials(username, password);
            Logger.Log("Signing in with " + username + ":" + password + "...");

            SubmitBtn.IsEnabled = false;
            TokenRefreshResult refreshResult = await _tokenProvider.RefreshAsync(credentials);
            if(refreshResult.Success)
            {
                OnLoggedIn?.Invoke();   
                Logger.Log("Obtained token: " + _tokenProvider.Token.AccessToken);
            }
            else
            {
                ErrorText.Text = refreshResult.RequestError ?? refreshResult.ResponseError;
                SubmitBtn.IsEnabled = true;
                Logger.Log("Failed to obtain token");
            }
        }
    }
}
