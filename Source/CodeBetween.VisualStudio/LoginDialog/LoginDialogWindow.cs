﻿using CodeBetween.VisualStudio.Client;
using Microsoft.VisualStudio.PlatformUI;

namespace CodeBetween.VisualStudio.LoginDialog
{
    /// <summary>
    /// 
    /// </summary>
    internal class LoginDialogWindow : DialogWindow
    {
        internal LoginDialogWindow()
        {
            IConfiguration config = new Configuration
            {
                CodeBetweenTokenEndpoint = "http://localhost/CodeBetween/token",
                EncryptionPassPhrase = "test123",
                CredentialsPath = @"C:\temp\codebetween.credentials"
            };
            TokenProvider tokenProvider = new TokenProvider(config);

            var content = new LoginControl(config, tokenProvider);
            content.OnLoggedIn += () => Close();

            Width = 300;
            Height = 200;
            Title = "CodeBetween Login";
            Content = content;
        }
    }
}
