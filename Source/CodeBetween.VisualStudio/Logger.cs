﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace CodeBetween.VisualStudio
{
    /// <summary>
    /// Very basic logging to a file.
    /// </summary>
    internal class Logger
    {
        static string _logFile = "C:\\temp\\logs\\CodeBetween_prototype_" + Process.GetCurrentProcess().Id + ".txt";
        
        public static void Log(string message)
        {
            message = "\n" + DateTime.Now.ToString("G") + "\t" + message;
            using (var stream = new FileStream(
                _logFile, FileMode.OpenOrCreate, FileSystemRights.AppendData, 
                FileShare.Write, 4096, FileOptions.None))
            using (var streamWriter = new StreamWriter(stream))
            {
                streamWriter.WriteLine(message);
            }
        }

        public static void Log(string message, params object[] prams)
        {
            StringBuilder msg = new StringBuilder(message);
            foreach(var pram in prams)
            {
                try
                {
                    string json = JsonConvert.SerializeObject(pram, Formatting.Indented);
                    msg.AppendLine(json);
                }
                catch(Exception ex)
                {
                    Log(ex);
                }
            }
            Log(msg.ToString());
        }

        public static void Log(Exception ex)
        {
            StringBuilder msg = new StringBuilder(ex.Message + "\n");
            msg.AppendLine(ex.StackTrace);
            Log(msg.ToString());
        }
    }
}
