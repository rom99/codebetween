﻿using Microsoft.AspNet.SignalR.Client;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using System;
using System.Threading;

using CodeBetween.Lib;

using static CodeBetween.VisualStudio.Logger;

namespace CodeBetween.VisualStudio
{
    public class EditorBufferUpdater
    {
        private IWpfTextView _textView;
        private bool _updating = false;

        private HubConnection _hubCx;
        private IHubProxy _hub;

        private SynchronizationContext _synchronizationContext;

        public EditorBufferUpdater(IWpfTextView textView)
        {
            Log("Creating Updater...");
            _textView = textView;
            _textView.TextBuffer.Changed += TextBuffer_Changed;
            _textView.TextBuffer.PostChanged += TextBuffer_PostChanged;
            Log("Done creating Updater.");
        }

        public async void Initialise()
        {
            Log("Creating signalr hub...");
            try
            {
                _hubCx = new HubConnection("http://localhost:61704/signalr");
                _hub = _hubCx.CreateHubProxy("CodeBetweenSessionHub");
                await _hubCx.Start();
                _hub.On<TextUpdate>("sessionUpdate", OnSessionUpdate);
                await _hub.Invoke("SubscribeToSession", "test-session");
            }
            catch (Exception ex)
            {
                Log(ex);
            }
            Log("Done creating signalr hub.");

            _synchronizationContext = SynchronizationContext.Current;
        }

        private void OnSessionUpdate(TextUpdate update)
        {
            Log("OnSessionUpdate with:", update);
            try
            {
                _synchronizationContext.Post(UpdateTextBufferOnUIThread, update);
            }
            catch(Exception ex)
            {
                Log(ex);
            }
        }

        private void UpdateTextBufferOnUIThread(object state)
        {
            Log("Updating text buffer...");
            try
            {
                TextUpdate update = state as TextUpdate;
                var edit = _textView.TextBuffer.CreateEdit();

                if (!string.IsNullOrEmpty(update.Removed))
                {
                    _updating = true;
                    edit.Delete(new Span(update.RemovedAt, update.Removed.Length));
                }

                if (!string.IsNullOrEmpty(update.Added))
                {
                    _updating = true;
                    edit.Insert(update.AddedAt, update.Added);
                }
                edit.Apply();
            }
            catch(Exception ex)
            {
                Log(ex);
            }
            Log("Done updating text buffer.");
        }

        private void TextBuffer_PostChanged(object sender, EventArgs e)
        {
            _updating = false;
        }

        private void TextBuffer_Changed(object sender, TextContentChangedEventArgs e)
        {
            Log("TextBuffer_Changed, handling...");
            if (e.Changes != null && !_updating)
            {
                _updating = true;
                foreach (var change in e.Changes)
                {
                    var update = new TextUpdate();
                    if(!string.IsNullOrEmpty(change.OldText))
                    {
                        update.Removed = change.OldText;
                        update.RemovedAt = change.OldSpan.Start;
                    }
                    if(!string.IsNullOrEmpty(change.NewText))
                    {
                        update.Added = change.NewText;
                        update.AddedAt = change.NewSpan.Start; 
                    }
                    Log("Invoking hub with: ", update);
                    _hub.Invoke("SessionUpdate", "test-session", update);
                }
            }
            Log("Finished handling TextBuffer_Changed");
        }
    }
}
