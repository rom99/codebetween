﻿using CodeBetween.VisualStudio;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeBetween.VisualStudio
{
    /// <summary>
    /// see https://stackoverflow.com/questions/6751086/visual-studio-text-editor-extension
    /// </summary>
    [ContentType("code")]
    [Export(typeof(IWpfTextViewCreationListener))]
    [TextViewRole(PredefinedTextViewRoles.Editable)]
    public sealed class TextViewCreationListener : IWpfTextViewCreationListener
    {
        public void TextViewCreated(IWpfTextView textView)
        {
            var updater = new EditorBufferUpdater(textView);
            updater.Initialise();
        }
    }
}
