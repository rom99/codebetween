# CodeBetween - develop better together

- Peer programming for remote developers.
- Work on the same code base at the same time.
- Screensharing without the lag
- Follow mode, or take control over your own view (scrolling, intellisense, whichever document you are viewing)

## Initial prototype

- Visual Studio extension that will live update a document in sync with another user editing the same document across a network.