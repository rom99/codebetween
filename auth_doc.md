CodeBetween connection flow

getUserAuth
- get user authentication data including permissions and other metadata
- this can also be used to check if the user's token is current
- refresh token if required

refreshToken
- get a new token using the saved credentials
- if it works, no problem
- if it doesn't work, why?
	- not authorised? user may try new credentials
	- server problem? show an appropriate error message

saveCredentials
- save users credentials to a file

getCredentials
- get users credentials from the file


Classes:

Configuration
+ CredentialsPath : string
+ CodeBetweenTokenEndpoint : string

UserCredentials
- credentialsPath
+ UserName : string
+ Password : string
+ Save(this: Credentials)
+ Get(credentialsPath: string) : UserCredentials (static)

UserCredentialsFileModel
+ UserName: string
+ Password: encrypted string

TokenProvider
+ Refresh() : Token
+ Get() : Token

Token
+ Value : String
+ Expired: Boolean
+ Expires: DateTime

UserInterface:

CredentialsForm
ConnectionStatus
TokenExpiredError
BadCredentialsError

Communication:

RecoveryManager
+ SynchroniseSources(sourceId)